# pep517_install

This tool implements minimal hooks into PEP517 build system to build wheels
and handles unpacking of said wheel into site-packages. It's meant primarily
to be a bootstrapping mechanism in a PEP517-only world

## Installing

make install

## Usage

pep517_install --prefix staging_directory
